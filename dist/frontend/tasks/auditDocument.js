/*jshint node:true, laxbreak:true */
'use strict';

module.exports = function(grunt) {
    require('jit-grunt')(grunt, {
        yuidoc: 'grunt-contrib-yuidoc'
    });

    grunt.config.merge({
        yuidoc: {
            compile: {
                name: '<%= pkg.name %>',
                description: '<%= pkg.description %>',
                version: '<%= pkg.version %>',
                url: '<%= pkg.homepage %>',
                options: {
                    paths: [
                        '<%= env.DIR_SRC %>'
                    ],
                    outdir: '<%= env.DIR_REPORTS_DOC %>',
                    themedir: '<%= env.DIR_CACHE %>/yuidoc-theme'
                }
            }
        }
    });

    grunt.registerTask('auditDocument',
        'Audit: Documentation via Yui Sub Task',
        [
            'yuidoc:compile'
        ]
    );
};