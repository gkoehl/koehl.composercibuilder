/*jshint node:true, laxbreak:true */
'use strict';

module.exports = function(grunt) {
    require('jit-grunt')(grunt, {
        scsslint: 'grunt-scss-lint',
        csslint: 'grunt-contrib-csslint',
        htmllint: 'grunt-html',
        jshint: 'grunt-contrib-jshint'
    });

    grunt.config.merge({
        scsslint: {
            allFiles: [
                '<%= env.DIR_SRC %>/**/*.scss',
            ],
            options: {
                bundleExec: false,
                config: null,
                reporterOutput: '<%= env.DIR_REPORTS_SCSS %>/lint-report.xml',
                colorizeOutput: true,
                maxBuffer: 300 * 1024 * 9999
            }
        },

        csslint: {
            options: {
                csslintrc: '<%= env.DIR_INCLUDES %>/.csslintrc',
                formatters: [
                    {
                        id: 'text',
                        dest: '<%= env.DIR_REPORTS_CSS %>/text-report.xml'
                    },
                    {
                        id: 'compact',
                        dest: '<%= env.DIR_REPORTS_CSS %>/compact-report.xml'
                    },
                    {
                        id: 'lint-xml',
                        dest: '<%= env.DIR_REPORTS_CSS %>/lint-xml-report.xml'
                    },
                    {
                        id: 'csslint-xml',
                        dest: '<%= env.DIR_REPORTS_CSS %>/csslint-xml-report.xml'
                    },
                    {
                        id: 'checkstyle-xml',
                        dest: '<%= env.DIR_REPORTS_CSS %>/checkstyle-xml-report.xml'
                    },
                    {
                        id: 'junit-xml',
                        dest: '<%= env.DIR_REPORTS_CSS %>/junit-xml-report.xml'
                    }
                ]
            },
            strict: {
                options: {
                    'import': 2
                },
                src: [
                    '<%= env.DIR_SRC %>/**/*.css'
                ]
            }
        },

        htmllint: {
            options: {
                reporter: 'checkstyle',
                reporterOutput: '<%= env.DIR_REPORTS_HTML %>/lint-report.xml'
            },
            all: [
                '<%= env.DIR_SRC %>/**/*.{htm,html}'
            ]
        },

        jshint: {
            lintScripts: {
                options: {
                    jshintrc: '<%= env.DIR_INCLUDES %>/.jshintrc',
                    reporter: 'checkstyle',
                    reporterOutput: '<%= env.DIR_REPORTS_JS %>/lint-report.xml'
                },
                src: [
                    '<%= env.DIR_SRC %>/**/*.js',
                    '!<%= env.DIR_SRC %>/**/*.min.js'
                ]
            }
        }
    });

    grunt.registerTask('auditLint', 
        'Audit: Linting codebase',
        [
            'force:on',
            'scsslint',
            'csslint:strict',
            'htmllint',
            'jshint:lintScripts',
            'force:reset'
        ]
    );
};