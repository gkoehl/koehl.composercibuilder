/*jshint node:true, laxbreak:true */
'use strict';

module.exports = function(grunt) {
    var PKG = require('./package.json');
    var ENV = require('./Gruntenv');

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt)({
        customTasksDir: 'tasks'
    });

    grunt.initConfig({
        pkg: PKG,
        env: ENV,
        clean: {
            options: {
                force: '<%= env.UNSAFE_MODE %>'
            },
            reports: [
                '<%= env.DIR_REPORTS %>'
            ],
            installed: [
                '<%= env.DIR_NPM %>'
            ]
        },
    });

    // Default Task
    grunt.registerTask(
        'default',
        'Run default tasks.',
        [
            'audit'
        ]
    );

    // Audit Task
    grunt.registerTask(
        'audit',
        'Runs a code audit on the given codebase',
        [
            'auditInit',
            'auditLint',
            'auditDocument'
        ]
    );

    // Audit Sub Tasks

    // Audit Initialization Sub Task
    grunt.registerTask(
        'auditInit',
        'Audit: Initializing',
        [
            'clean:reports'
        ]
    );

    // Audit Clean Sub Task
    grunt.registerTask(
        'audit:clean',
        'Audit: Cleaning up',
        []
    );
};