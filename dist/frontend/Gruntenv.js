'use strict';

/**
 * Environment custom constants.
 * Update with path to static js src directory.
 *
 * @var CUSTOM_DIR_SRC
 * @static
 */
var CUSTOM_DIR_SRC = '.@FILTERTOKEN@filters.frontend.src.dir@FILTERTOKEN@';

/**
 * Environment constants.
 *
 * @class Config
 * @static
 */
var Config = {
    DIR_SRC: CUSTOM_DIR_SRC,
    DIR_NPM: 'node_modules',
    DIR_CACHE: 'cache',
    DIR_TMP: '.tmp',
    DIR_INCLUDES: 'includes',
    DIR_TASKS: 'tasks',
    DIR_REPORTS: 'reports',
    UNSAFE_MODE: false
};

Config.DIR_SRC_SCSS = Config.DIR_SRC + '/scss';
Config.DIR_SRC_CSS = Config.DIR_SRC + '/css';
Config.DIR_SRC_HTML = Config.DIR_SRC + '/templates';
Config.DIR_SRC_JS = Config.DIR_SRC + '/js';

Config.DIR_REPORTS_DOC = Config.DIR_REPORTS + '/doc';
Config.DIR_REPORTS_SCSS = Config.DIR_REPORTS + '/scss';
Config.DIR_REPORTS_CSS = Config.DIR_REPORTS + '/css';
Config.DIR_REPORTS_HTML = Config.DIR_REPORTS + '/html';
Config.DIR_REPORTS_JS = Config.DIR_REPORTS + '/js';

module.exports = Config;